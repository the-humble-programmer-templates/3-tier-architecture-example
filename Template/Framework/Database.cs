using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Newtonsoft.Json;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace MVC_Example.MVCFramework
{
    public class Database
    {
        private readonly string _local = "database";

        public async Task Save(string table, string key, object a)
        {
            var data = JsonConvert.SerializeObject(a);
            File.WriteAllText($"./database/{table}_{key}", data);
            await UpdateIndex(table, key, true);
        }

        public async Task<string[]> TableKeys(string table)
        {
            return (await LoadIndex(table)).Where((k) => k.Value).Select(v => v.Key).ToArray();
        }

        public async Task<T> Load<T>(string table, string key) where T : class
        {
            if (!await Exist(table, key)) return null;
            var s = File.ReadAllText($"./database/{table}_{key}");
            return JsonConvert.DeserializeObject<T>(s);
        }

        public async Task Delete(string table, string key)
        {
            File.Delete($"./database/{table}_{key}");
            await UpdateIndex(table, key, false);
        }

        public async Task<bool> Exist(string table, string key)
        {
            var index = await LoadIndex(table);
            return index.ContainsKey(key) && index[key];
        }

        private async Task UpdateIndex(string table, string key, bool val)
        {
            var x = await LoadIndex(table);
            x[key] = val;
            await SaveIndex(table, x);
        }

        private async Task SaveIndex(string table, Dictionary<string, bool> indexes)
        {
            var val = JsonConvert.SerializeObject(indexes);
            await File.WriteAllTextAsync($"./{_local}/" + table, val);
        }

        private async Task<Dictionary<string, bool>> LoadIndex(string table)
        {
            var indexes = File.Exists($"./{_local}/" + table)
                ? (await File.ReadAllTextAsync($"./{_local}/" + table))
                : "{}";
            return JsonConvert.DeserializeObject<Dictionary<string, bool>>(indexes);
        }
    }
}