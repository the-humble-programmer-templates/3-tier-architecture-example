using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace MVC_Example
{
    public static class Util
    {
        public static IEnumerable<string> ValidateGuid(this string id)
        {
            try
            {
                var _ = Guid.Parse(id);
                return new string[] { };
            }
            catch (ArgumentNullException)
            {
                return new[] {"null id"};
            }
            catch (FormatException)
            {
                return new[] {"id bad format"};
            }
        }

        public static void Log(this object all)
        {
            Console.WriteLine(JsonConvert.SerializeObject(all));
        }
    }
}