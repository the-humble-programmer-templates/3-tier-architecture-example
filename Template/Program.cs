﻿using System;
using System.Threading.Tasks;
using MVC_Example.MVCFramework;
using MVC_Example.Shelf.Controllers;
using MVC_Example.Shelf.Repository;
using MVC_Example.Shelf.Service;
using MVC_Example.Shelf.View;

namespace MVC_Example
{
    class Program
    {
        static void Main(string[] args)
        {
            AsyncMain(args).GetAwaiter().GetResult();
        }

        async static Task AsyncMain(string[] args)
        {
            //Manual Dependency Injection
            var db = new Database();
            // Construct repositories and apply dependency
            var authorRepository = new AuthorRepository(db);
            var bookRepository = new BookRepository(db);
            authorRepository.BookRepository = bookRepository;
            bookRepository.AuthorRepository = authorRepository;

            // Create views
            var authorView = new AuthorView();
            var bookView = new BookView();

            // Create validators
            var bookValidator = new BookValidator();
            var authorValidator = new AuthorValidator();

            // Create services
            var bookService = new BookService(bookRepository, authorRepository);
            var authorService = new AuthorService(authorRepository);

            // Create Mappers
            var bookMapper = new BookMapper(bookService);
            var authorMapper = new AuthorMapper(authorService);

            // Controllers
            var bookController = new BookController(bookService, bookMapper, bookValidator, bookView);
            var authorController = new AuthorController(authorService, authorValidator, authorMapper, authorView);

            // Routing
            var router = new Router(new object[] { bookController, authorController});
            var (success, message) = await router.Route(args);
            if (!success)
            {
                Console.WriteLine(message);
            }
            else
            {
                Console.WriteLine(message);
            }
        }
    }
}