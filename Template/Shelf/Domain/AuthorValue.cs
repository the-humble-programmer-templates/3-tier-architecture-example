namespace MVC_Example.Shelf.Domain
{
    public class AuthorValue
    {
        public AuthorValue(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public string Name { get; set; }
        public int Age { get; set; }
    }
}