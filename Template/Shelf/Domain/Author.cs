using System;
using System.Collections.Generic;

namespace MVC_Example.Shelf.Domain
{
    public class Author
    {
        public Author(Guid id, AuthorValue detail)
        {
            Id = id;
            Detail = detail;
            Books = new Book[] { };
        }

        public Guid Id { get; set; }
        public AuthorValue Detail { get; set; }
        public IEnumerable<Book> Books { get; set; }
    }
}