using System;

namespace MVC_Example.Shelf.Domain
{
    public class BookValue
    {
        public BookValue(string title, string description)
        {
            Title = title;
            Description = description;
        }

        public string Title { get; set; }
        public string Description { get; set; }
    }
}