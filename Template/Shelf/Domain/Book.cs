using System;

namespace MVC_Example.Shelf.Domain
{
    public class Book
    {
        public Book(BookValue detail, Guid id, Author author)
        {
            Detail = detail;
            Id = id;
            Author = author;
        }

        public BookValue Detail { get; set; }
        public Guid Id { get; set; }
        public Author Author { get; set; }
    }
}