using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MVC_Example.Shelf.Domain;
using MVC_Example.Shelf.Repository;

namespace MVC_Example.Shelf.Service
{
    public class AuthorService : IAuthorService
    {
        private readonly IAuthorRepository _repository;

        public AuthorService(IAuthorRepository repository)
        {
            _repository = repository;
        }

        public Task<Author> Create(AuthorValue author)
        {
            return _repository.Create(author);
        }

        public async Task<Author> Update(Guid id, AuthorValue author)
        {
            return await _repository.Update(id, author);
        }

        public Task<bool> Delete(Guid id)
        {
            return _repository.Delete(id);
        }

        public Task<Author> Get(Guid id)
        {
            return _repository.Get(id);
        }

        public Task<IEnumerable<Author>> List()
        {
            return _repository.List();
        }
    }
}