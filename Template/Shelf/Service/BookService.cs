using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MVC_Example.Shelf.Domain;
using MVC_Example.Shelf.Repository;

namespace MVC_Example.Shelf.Service
{
    public class BookService : IBookService
    {
        private readonly IBookRepository _repository;
        private readonly IAuthorRepository _authorRepository;

        public BookService(IBookRepository repository, IAuthorRepository authorRepository)
        {
            _repository = repository;
            _authorRepository = authorRepository;
        }

        public async Task<Book> Create(Guid id, BookValue book)
        {
            var author = await _authorRepository.Get(id);
            if (author == null) return null;
            return await _repository.Create(book, author);
        }

        public Task<Book> Update(Guid id, BookValue book)
        {
            return _repository.Update(id, book);
        }

        public Task<bool> Delete(Guid id)
        {
            return _repository.Delete(id);
        }

        public Task<Book> Get(Guid id)
        {
            return _repository.Get(id);
        }

        public Task<IEnumerable<Book>> List()
        {
            return _repository.List();
        }
    }
}