using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MVC_Example.Shelf.Domain;

namespace MVC_Example.Shelf.Service
{
    public interface IAuthorService
    {
        Task<Author> Create(AuthorValue author);
        Task<Author> Update(Guid id, AuthorValue author);
        Task<bool> Delete(Guid id);
        Task<Author> Get(Guid id);
        Task<IEnumerable<Author>> List();
    }
}