using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MVC_Example.Shelf.Domain;

namespace MVC_Example.Shelf.Service
{
    public interface IBookService
    {
        Task<Book> Create(Guid authorId, BookValue book);
        Task<Book> Update(Guid id, BookValue book);
        Task<bool> Delete(Guid id);
        Task<Book> Get(Guid id);
        Task<IEnumerable<Book>> List();
    }
}