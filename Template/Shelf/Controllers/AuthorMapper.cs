using System;
using System.Linq;
using System.Threading.Tasks;
using MVC_Example.Shelf.Domain;
using MVC_Example.Shelf.Service;
using MVC_Example.Shelf.ViewModel;

namespace MVC_Example.Shelf.Controllers
{
    public class AuthorMapper
    {
        private readonly IAuthorService _service;

        public AuthorMapper(IAuthorService service)
        {
            _service = service;
        }

        public AuthorValue CreateModelToValue(string name, string age)
        {
            var a = int.Parse(age);
            return new AuthorValue(name, a);
        }

        public async Task<AuthorValue> UpdateNameModel(string id, string name)
        {
            var author = await _service.Get(new Guid(id));
            author.Detail.Name = name;
            return author.Detail;
        }

        public async Task<AuthorValue> UpdateAgeModel(string id, string age)
        {
            var author = await _service.Get(new Guid(id));
            author.Detail.Age = int.Parse(age);
            return author.Detail;
        }

        public AuthorViewModel ToViewModel(Author auth)
        {
            if (auth == null) return null;
            return new AuthorViewModel(auth.Id, auth.Detail.Name, auth.Detail.Age,
                auth.Books.Select(x => x.Detail.Title));
        }
    }
}