using System.Collections.Generic;
using System.Linq;

namespace MVC_Example.Shelf.Controllers
{
    public class BookValidator
    {
        public (bool, IEnumerable<string>) ValidateCreate(string title, string description, string author)
        {
            var error = author.ValidateGuid();
            if (string.IsNullOrWhiteSpace(title)) error = error.Append("title cannot be empty");
            if (string.IsNullOrWhiteSpace(description)) error = error.Append("description cannot be description");
            return (!error.Any(), error);
        }

        public (bool, IEnumerable<string>) ValidateTitleUpdate(string id, string title)
        {
            var error = id.ValidateGuid();
            if (string.IsNullOrWhiteSpace(title)) error = error.Append("title cannot be empty");
            return (!error.Any(), error);
        }

        public (bool, IEnumerable<string>) ValidateDescriptionUpdate(string id, string description)
        {
            var error = id.ValidateGuid();
            if (string.IsNullOrWhiteSpace(description)) error = error.Append("description cannot be description");
            return (!error.Any(), error);
        }

        public (bool, IEnumerable<string>) ValidateDelete(string id)
        {
            var error = id.ValidateGuid();
            return (!error.Any(), error);
        }

        public (bool, IEnumerable<string>) ValidateGet(string id)
        {
            var error = id.ValidateGuid();
            return (!error.Any(), error);
        }
    }
}