using System;
using System.Linq;
using System.Threading.Tasks;
using MVC_Example.MVCFramework;
using MVC_Example.Shelf.Service;
using MVC_Example.Shelf.View;

namespace MVC_Example.Shelf.Controllers
{
    [Controller("author")]
    public class AuthorController
    {
        private readonly IAuthorService _service;
        private readonly AuthorMapper _map;
        private readonly AuthorValidator _valid;
        private readonly IAuthorView _view;

        public AuthorController(IAuthorService service, AuthorValidator valid, AuthorMapper map, IAuthorView view)
        {
            _service = service;
            _valid = valid;
            _map = map;
            _view = view;
        }

        [Endpoint("create {name} {age}")]
        public async Task<string> Create(string name, string age)
        {
            // Validate input and return
            var (valid, errors) = _valid.ValidateCreate(name, age);
            if (!valid) return _view.Errors(errors);

            // Map between input and domain values
            var aVal = _map.CreateModelToValue(name, age);

            // Use the domain model to use services
            var resp = await _service.Create(aVal);

            // Map between domain and ouput
            var viewModel = _map.ToViewModel(resp);

            //Render the view
            return _view.Create(viewModel);
        }

        [Endpoint("update {id} name {name}")]
        public async Task<string> UpdateName(string id, string name)
        {
            // Validate input and return
            var (valid, errors) = _valid.ValidateUpdateName(id, name);
            if (!valid) return _view.Errors(errors);

            // Map between input and domain values
            var aVal = await _map.UpdateNameModel(id, name);

            // Use the domain model to use services
            var resp = await _service.Update(new Guid(id), aVal);

            // Map between domain and ouput
            var viewModel = _map.ToViewModel(resp);

            //Render the view
            return _view.Update(viewModel);
        }


        [Endpoint("update {id} age {age}")]
        public async Task<string> UpdateAge(string id, string age)
        {
            // Validate input and return
            var (valid, errors) = _valid.ValidateUpdateAge(id, age);
            if (!valid) return _view.Errors(errors);

            // Map between input and domain values
            var aVal = await _map.UpdateAgeModel(id, age);

            // Use the domain model to use services
            var resp = await _service.Update(new Guid(id), aVal);

            // Map between domain and ouput
            var viewModel = _map.ToViewModel(resp);

            //Render the view
            return _view.Update(viewModel);
        }

        [Endpoint("del {id}")]
        public async Task<string> Delete(string id)
        {
            // Validate input and return
            var (valid, errors) = _valid.ValidateDelete(id);
            if (!valid) return _view.Errors(errors);

            // Use the domain model to use services
            var resp = await _service.Delete(new Guid(id));

            //Render the view
            return _view.Delete(resp);
        }

        [Endpoint("get {id}")]
        public async Task<string> Get(string id)
        {
            // Validate input and return
            var (valid, errors) = _valid.ValidateGet(id);
            if (!valid) return _view.Errors(errors);

            // Use the domain model to use services
            var resp = await _service.Get(new Guid(id));

            // Map between domain and ouput
            var viewModel = _map.ToViewModel(resp);

            //Render the view
            return _view.Get(viewModel);
        }

        [Endpoint("list")]
        public async Task<string> List()
        {
            // Use the domain model to use services
            var resp = await _service.List();

            // Map between domain and ouput
            var viewModel = resp.Select(x => _map.ToViewModel(x));
            //Render the view
            return _view.List(viewModel);
        }
    }
}