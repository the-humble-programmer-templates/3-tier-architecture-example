using System;
using System.Linq;
using System.Threading.Tasks;
using MVC_Example.MVCFramework;
using MVC_Example.Shelf.Service;
using MVC_Example.Shelf.View;

namespace MVC_Example.Shelf.Controllers
{
    [Controller("book")]
    public class BookController
    {
        private readonly IBookService _service;
        private readonly BookMapper _bookMapper;
        private readonly BookValidator _validator;
        private readonly IBookView _view;

        public BookController(IBookService service, BookMapper bookMapper, BookValidator validator, IBookView view)
        {
            _service = service;
            _bookMapper = bookMapper;
            _validator = validator;
            _view = view;
        }

        [Endpoint("create {title} {description} {author}")]
        public async Task<string> Create(string title, string description, string author)
        {
            //Validation
            var (valid, errors) = _validator.ValidateCreate(title, description, author);
            if (!valid) return _view.Errors(errors);

            // Map from input to domain
            var domain = _bookMapper.FromCreateModel(title, description);

            // Interact with domain layer via service
            var resp = await _service.Create(new Guid(author), domain);

            // Convert to view model
            var viewModel = _bookMapper.ToViewModel(resp);

            // View renders 
            return _view.Create(viewModel);
        }

        [Endpoint("update {id} title {title}")]
        public async Task<string> UpdateTitle(string id, string title)
        {
            //Validation
            var (valid, errors) = _validator.ValidateTitleUpdate(id, title);
            if (!valid) return _view.Errors(errors);

            // Map from input to domain
            var domain = await _bookMapper.FromUpdateTitleModel(id, title);

            // Interact with domain layer via service
            var resp = await _service.Update(new Guid(id), domain);

            // Convert to view model
            var viewModel = _bookMapper.ToViewModel(resp);

            // View renders 
            return _view.Update(viewModel);
        }


        [Endpoint("update {id} desc {desc}")]
        public async Task<string> UpdateDescription(string id, string desc)
        {
            //Validation
            var (valid, errors) = _validator.ValidateDescriptionUpdate(id, desc);
            if (!valid) return _view.Errors(errors);

            // Map from input to domain
            var domain = await _bookMapper.FromUpdateDescriptionModel(id, desc);

            // Interact with domain layer via service
            var resp = await _service.Update(new Guid(id), domain);

            // Convert to view model
            var viewModel = _bookMapper.ToViewModel(resp);

            // View renders 
            return _view.Update(viewModel);
        }

        [Endpoint("del {id}")]
        public async Task<string> Delete(string id)
        {
            //Validation
            var (valid, errors) = _validator.ValidateDelete(id);
            if (!valid) return _view.Errors(errors);

            // Map from input to domain
            var domain = _bookMapper.FromDeleteModel(id);

            // Interact with domain layer via service
            var success = await _service.Delete(domain);
            // View renders 
            return _view.Delete(success);
        }

        [Endpoint("get {id}")]
        public async Task<string> Get(string id)
        {
            //Validation
            var (valid, errors) = _validator.ValidateGet(id);
            if (!valid) return _view.Errors(errors);

            // Map from input to domain
            var domain = _bookMapper.FromGetModel(id);

            // Interact with domain layer via service
            var resp = await _service.Get(domain);

            // Convert from domain model to view model
            var viewModel = _bookMapper.ToViewModel(resp);

            // View renders 
            return _view.Get(viewModel);
        }

        [Endpoint("list")]
        public async Task<string> List()
        {
            var all = await _service.List();

            // Convert from domain model to view model
            var viewModel = all.Select(x => _bookMapper.ToViewModel(x));

            // View renders 
            return _view.List(viewModel);
        }
    }
}