using System;
using System.Threading.Tasks;
using MVC_Example.Shelf.Domain;
using MVC_Example.Shelf.Service;
using MVC_Example.Shelf.ViewModel;
using Newtonsoft.Json;

namespace MVC_Example.Shelf.Controllers
{
    public class BookMapper
    {
        private readonly IBookService _service;

        public BookMapper(IBookService service)
        {
            _service = service;
        }

        public BookValue FromCreateModel(string title, string description)
        {
            return new BookValue(title, description);
        }

        public async Task<BookValue> FromUpdateDescriptionModel(string id, string desc)
        {
            var guid = new Guid(id);
            var book = await _service.Get(guid);
            book.Detail.Description = desc;
            return book.Detail;
        }

        public async Task<BookValue> FromUpdateTitleModel(string id, string title)
        {
            var guid = new Guid(id);
            var book = await _service.Get(guid);
            book.Detail.Title = title;
            return book.Detail;
        }

        public Guid FromDeleteModel(string id)
        {
            return new Guid(id);
        }


        public Guid FromGetModel(string id)
        {
            return new Guid(id);
        }

        public BookViewModel ToViewModel(Book book)
        {
            if (book == null) return null;
            return new BookViewModel(book.Id, book.Detail.Description, book.Detail.Title, book.Author.Detail.Name);
        }
    }
}