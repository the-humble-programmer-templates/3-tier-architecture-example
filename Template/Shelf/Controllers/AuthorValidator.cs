using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

namespace MVC_Example.Shelf.Controllers
{
    public class AuthorValidator
    {
        public (bool, IEnumerable<string>) ValidateCreate(string _, string age)
        {
            var errors = ValidateAge(age);
            return (!errors.Any(), errors);
        }

        public (bool, IEnumerable<string>) ValidateUpdateName(string id, string _)
        {
            var errors = id.ValidateGuid();
            return (!errors.Any(), errors);
        }

        public (bool, IEnumerable<string>) ValidateUpdateAge(string id, string age)
        {
            var errors = id.ValidateGuid();
            errors = errors.Concat(ValidateAge(age));
            return (!errors.Any(), errors);
        }

        public (bool, IEnumerable<string>) ValidateDelete(string id)
        {
            var errors = id.ValidateGuid();
            return (!errors.Any(), errors);
        }


        public (bool, IEnumerable<string>) ValidateGet(string id)
        {
            var errors = id.ValidateGuid();
            return (!errors.Any(), errors);
        }


        

        private IEnumerable<string> ValidateAge(string age)
        {
            try
            {
                var errors = new List<string>();
                var x = int.Parse(age);
                if (x > 120)
                {
                    errors.Add("age too old");
                }

                if (x < 0)
                {
                    errors.Add("no negative age");
                }

                return errors;
            }
            catch
            {
                return new[] {"age needs to be integer"};
            }
        }
    }
}