using System;
using System.Collections.Generic;
using MVC_Example.Shelf.Domain;

namespace MVC_Example.Shelf.Data
{
    public class AuthorData
    {

        public AuthorData(Guid id, string name, int age, IEnumerable<Guid> books)
        {
            Id = id;
            Name = name;
            Age = age;
            Books = books;
        }

        public Guid Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public IEnumerable<Guid> Books { get; set; }
    }
}