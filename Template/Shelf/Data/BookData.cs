using System;
using MVC_Example.Shelf.Domain;

namespace MVC_Example.Shelf.Data
{
    public class BookData
    {
        public BookData(string title, string description, Guid id, Guid authorId)
        {
            Title = title;
            Description = description;
            Id = id;
            AuthorId = authorId;
        }

        public BookData()
        {
        }

        public string Title { get; set; }
        public string Description { get; set; }
        public Guid Id { get; set; }
        public Guid AuthorId { get; set; }
    }
}