using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MVC_Example.MVCFramework;
using MVC_Example.Shelf.Data;
using MVC_Example.Shelf.Domain;

namespace MVC_Example.Shelf.Repository
{
    public class AuthorRepository : IAuthorRepository
    {
        private readonly Database _db;
        private const string TableKey = "author";
        public IBookRepository BookRepository { get; set; }

        public AuthorRepository(Database db)
        {
            _db = db;
        }

        public async Task<Author> Get(Guid id)
        {
            var data = await _db.Load<AuthorData>(TableKey, id.ToString());
            return await ToDomainModel(data);
        }

        public async Task<Author> Update(Guid id, AuthorValue author)
        {
            var domain = await Get(id);
            if (domain == null) return null;
            domain.Detail = author;
            var data = ToDataModel(domain);
            await _db.Save(TableKey, data.Id.ToString(), data);
            return domain;
        }


        public async Task<Author> Create(AuthorValue a)
        {
            var author = new Author(Guid.NewGuid(), a);
            var data = ToDataModel(author);
            await _db.Save(TableKey, data.Id.ToString(), data);
            return author;
        }

        public async Task<bool> Delete(Guid id)
        {
            try
            {
                var exist = await _db.Exist(TableKey, id.ToString());
                if (!exist)
                    return false;

                var data = await _db.Load<AuthorData>(TableKey, id.ToString());

                // Fake cascade delete
                foreach (var book in data.Books)
                    await BookRepository.Delete(book);

                // Delete main
                await _db.Delete(TableKey, id.ToString());
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<IEnumerable<Author>> List()
        {
            var keys = await _db.TableKeys(TableKey);
            var bookDatas = await Task.WhenAll(keys.Select(x => _db.Load<AuthorData>(TableKey, x)));
            return await Task.WhenAll(bookDatas.Select(ToDomainModel));
        }

        public async Task<bool> RemoveBook(Author author, Book book)
        {
            author.Books = author.Books.Where(x => x.Id != book.Id);
            var data = ToDataModel(author);
            await _db.Save(TableKey, data.Id.ToString(), data);
            return true;
        }

        public async Task<bool> AddBook(Author author, Book book)
        {
            author.Books = author.Books.Append(book);
            var data = ToDataModel(author);

            await _db.Save(TableKey, data.Id.ToString(), data);
            return true;
        }

        private AuthorData ToDataModel(Author author)
        {
            return author == null
                ? null
                : new AuthorData(author.Id, author.Detail.Name, author.Detail.Age, author.Books.Select(x => x.Id));
        }

        private async Task<Author> ToDomainModel(AuthorData data)
        {
            if (data == null) return null;
            var author = new Author(data.Id, new AuthorValue(data.Name, data.Age));
            var books = await Task.WhenAll(data.Books.Select(x => BookRepository.ShallowLoad(x, author)));
            author.Books = books;
            return author;
        }
    }
}