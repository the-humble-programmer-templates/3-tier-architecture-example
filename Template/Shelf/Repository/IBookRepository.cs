using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MVC_Example.Shelf.Domain;

namespace MVC_Example.Shelf.Repository
{
    public interface IBookRepository
    {
        Task<Book> Get(Guid id);

        Task<Book> Update(Guid id, BookValue  book);

        Task<Book> Create(BookValue value, Author author);

        Task<bool> Delete(Guid id);

        Task<IEnumerable<Book>> List();

        Task<Book> ShallowLoad(Guid id, Author author);
    }
}