using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MVC_Example.Shelf.Domain;

namespace MVC_Example.Shelf.Repository
{
    public interface IAuthorRepository
    {
        Task<Author> Get(Guid id);

        Task<Author> Update(Guid id, AuthorValue author);

        Task<Author> Create(AuthorValue author);

        Task<bool> Delete(Guid id);

        Task<IEnumerable<Author>> List();

        Task<bool> RemoveBook(Author author, Book book);
        Task<bool> AddBook(Author author, Book book);
    }
}