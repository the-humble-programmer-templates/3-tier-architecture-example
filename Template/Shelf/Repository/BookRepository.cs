using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using MVC_Example.MVCFramework;
using MVC_Example.Shelf.Data;
using MVC_Example.Shelf.Domain;

namespace MVC_Example.Shelf.Repository
{
    public class BookRepository : IBookRepository
    {
        private readonly Database _database;
        private const string TableKey = "book";
        public IAuthorRepository AuthorRepository { get; set; }

        public BookRepository(Database database)
        {
            _database = database;
        }


        public async Task<Book> Get(Guid id)
        {
            var data = await _database.Load<BookData>(TableKey, id.ToString());
            return await ToDomainModel(data);
        }


        public async Task<Book> ShallowLoad(Guid id, Author author)
        {
            var data = await _database.Load<BookData>(TableKey, id.ToString());
            var domain = ToShallowDomainModel(data);
            domain.Author = author;
            return domain;
        }

        public async Task<Book> Update(Guid id, BookValue book)
        {
            var domain = await Get(id);
            if (domain == null) return null;
            domain.Detail = book;
            var data = ToDataModel(domain);
            await _database.Save(TableKey, data.Id.ToString(), data);
            return domain;
        }

        public async Task<Book> Create(BookValue val, Author author)
        {
            var book = new Book(val, Guid.NewGuid(), author);
            var data = ToDataModel(book);
            var success = await AuthorRepository.AddBook(author, book);
            if (!success) return null;
            await _database.Save(TableKey, data.Id.ToString(), data);
            return book;
        }

        public async Task<bool> Delete(Guid id)
        {
            try
            {
                //Delatred relation
                var book = await Get(id);
                if (book == null) return false;
                await AuthorRepository.RemoveBook(book.Author, book);

                // Delete actual entry
                await _database.Delete(TableKey, id.ToString());
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<IEnumerable<Book>> List()
        {
            var keys = await _database.TableKeys(TableKey);
            var bookDatas = await Task.WhenAll(keys.Select(x => _database.Load<BookData>(TableKey, x)));
            return await Task.WhenAll(bookDatas.Select(ToDomainModel));
        }

        public BookData ToDataModel(Book book)
        {
            if (book == null) return null;
            return new BookData(book.Detail.Title, book.Detail.Description, book.Id, book.Author.Id);
        }

        public async Task<Book> ToDomainModel(BookData data)
        {
            if (data == null) return null;

            var author = await this.AuthorRepository.Get(data.AuthorId);
            return new Book(new BookValue(data.Title, data.Description), data.Id, author);
        }

        public Book ToShallowDomainModel(BookData data)
        {
            if (data == null) return null;
            return new Book(new BookValue(data.Title, data.Description), data.Id, null);
        }
    }
}