using System.Collections.Generic;
using MVC_Example.Shelf.ViewModel;

namespace MVC_Example.Shelf.View
{
    public interface IAuthorView
    {
        string Create(AuthorViewModel vm);
        string Get(AuthorViewModel vm);
        string Delete(bool success);
        string Update(AuthorViewModel vm);
        
        string List(IEnumerable<AuthorViewModel> vm);

        string Errors(IEnumerable<string> errors);

    }
}