using System.Collections.Generic;
using System.Linq;
using Kirinnee.Helper;
using MVC_Example.Shelf.ViewModel;

namespace MVC_Example.Shelf.View
{
    public class BookView : IBookView
    {
        public string Create(BookViewModel vm)
        {
            if (vm == null) return "Failed to create book.";
            return $"<{vm.Title}> book written by {vm.Author} has beee successfully created! " +
                   $"\nID: {vm.Id}";
        }

        public string Get(BookViewModel vm)
        {
            if (vm == null) return "Book does not exist";
            return $"Book:\n\tID: {vm.Id}\n\tTitle: {vm.Title}\n\tAuthor: {vm.Author}\n\tDescription: {vm.Description}";
        }

        public string Delete(bool success)
        {
            if (success)
                return "Book has been successfully deleted";
            else
                return "Book failed to be deleted, perhaps it didn't exist?'";
        }

        public string Update(BookViewModel vm)
        {
            if (vm == null)
            {
                return "Failed to update book details";
            }
            else
            {
                return $"{vm.Title} has been updated! Info:\n\tID: {vm.Id}\n\tDescription: {vm.Description}";
            }
        }

        public string List(IEnumerable<BookViewModel> vm)
        {
            if (!vm.Any())
            {
                return "There are no books right now";
            }
            else
            {
                return "Books: " + vm.Select(x => "\n\t" + x.Title + " by " + x.Author + ": " + x.Id).JoinBy("");
            }
        }

        public string Errors(IEnumerable<string> errors)
        {
            return "Invalid request: \n\t" + errors.JoinBy("\n\t");
        }
    }
}