using System.Collections.Generic;
using MVC_Example.Shelf.ViewModel;

namespace MVC_Example.Shelf.View
{
    public interface IBookView
    {
        string Create(BookViewModel vm);
        string Get(BookViewModel vm);
        string Delete(bool success);
        string Update(BookViewModel vm);

        string List(IEnumerable<BookViewModel> vm);

        string Errors(IEnumerable<string> errors);
    }
}