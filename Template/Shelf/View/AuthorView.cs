using System;
using System.Collections.Generic;
using System.Linq;
using Kirinnee.Helper;
using MVC_Example.Shelf.ViewModel;

namespace MVC_Example.Shelf.View
{
    public class AuthorView : IAuthorView
    {
        public string Create(AuthorViewModel vm)
        {
            if (vm == null) return "Failed to create author";
            return $"Author {vm.Name} has been created with {vm.Id}";
        }

        public string Get(AuthorViewModel vm)
        {
            if (vm == null) return "No such author or encountered other errors";
//            Console.WriteLine(vm.Books.Count());
            return $"Author:\n\tID :{vm.Id}\n\tName: {vm.Name}\n\tAge: {vm.Age}\n\tBook:\n\t\t- " +
                   vm.Books.JoinBy("\n\t\t- ");
        }

        public string Delete(bool success)
        {
            if (success)
            {
                return "Author has been successfully deleted";
            }

            return "Failed to delete author. Perhaps it does not exist?";
        }

        public string Update(AuthorViewModel vm)
        {
            return vm == null ? "Failed to update Author details" : $"The author {vm.Name} of ID {vm.Id} has been updated.";
        }

        public string List(IEnumerable<AuthorViewModel> vm)
        {
            if (!vm.Any())
            {
                return "There are no authors right now";
            }
            else
            {
                return "Authors: " + vm.Select(x => "\n\t" + x.Name  + ": " + x.Id).JoinBy("");
            }
        }

        public string Errors(IEnumerable<string> errors)
        {
            return "Invalid request: \n\t" + errors.JoinBy("\n\t");
        }
    }
}