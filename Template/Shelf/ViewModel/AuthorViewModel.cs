using System;
using System.Collections.Generic;

namespace MVC_Example.Shelf.ViewModel
{
    public class AuthorViewModel
    {
        public AuthorViewModel(Guid id, string name, int age, IEnumerable<string> books)
        {
            Id = id;
            Name = name;
            Age = age;
            Books = books;
        }

        public Guid Id { get; }
        public string Name { get; }
        public int Age { get; }
        public IEnumerable<string> Books { get; }
    }
}