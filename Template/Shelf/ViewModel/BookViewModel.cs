using System;

namespace MVC_Example.Shelf.ViewModel
{
    public class BookViewModel
    {
        public BookViewModel(Guid id, string description, string title, string author)
        {
            Id = id;
            Description = description;
            Title = title;
            Author = author;
        }

        public Guid Id { get; }
        public string Description { get; }
        public string Title { get; }
        public string Author { get; }
    }
}